import torch.nn as nn
import torch
from resnet import conv3x3, conv1x1


class RPN(nn.Module):
    def __init__(self, P2, P3, P4, P5, P6):
        super().__init__()
        self.P2 = (P2, 2)
        self.P3 = (P3, 3)
        self.P4 = (P4, 4)
        self.P5 = (P5, 5)
        self.P6 = (P6, 6)
        self.aspect_ratios = 3
        self.anchors_per_location = 1
        self.bbox_coordinates = 4
        self.relu = nn.ReLU(inplace=True)
        self.build_all_rpn_layers(p_numbers_list=[2, 3, 4, 5, 6])

    def build_all_rpn_layers(self, p_numbers_list):
        for p_num in p_numbers_list:
            self.build_single_rpn(p_num)

    def build_single_rpn(self, p_number):
        # common branch layers
        setattr(self, 'p'+str(p_number)+'conv_3x3', conv3x3(in_planes=256, out_planes=256))

        # obecness branch layers
        setattr(self, 'p'+str(p_number)+'objectness', conv1x1(in_planes=256, out_planes=self.aspect_ratios))
        setattr(self, 'p'+str(p_number)+'objectness_sigmoid', nn.Sigmoid())

        # bbox predictions branach
        setattr(self, 'p'+str(p_number)+'bbox_pred', conv1x1(in_planes=256, out_planes=int((self.aspect_ratios/self.anchors_per_location) * self.bbox_coordinates)))

    def forward(self):
        outputs_list = list()
        input_feature_maps = [self.P2, self.P3, self.P4, self.P5, self.P6]
        for input_map in input_feature_maps:

            out = getattr(self, 'p'+str(input_map[1])+'conv_3x3')(input_map[0])
            out = self.relu(out)

            # objectness
            out_obj = getattr(self, 'p'+str(input_map[1])+'objectness')(out)
            out_obj = torch.reshape(out_obj, (out_obj.shape[0], out_obj.shape[2] * out_obj.shape[3] * self.aspect_ratios))
            out_obj = getattr(self, 'p'+str(input_map[1])+'objectness_sigmoid')(out_obj)

            # bbox
            out_bbox = getattr(self, 'p'+str(input_map[1])+'bbox_pred')(out)
            out_bbox = torch.reshape(out_bbox, (out_bbox.shape[0], out_bbox.shape[2] * out_bbox.shape[3] * self.aspect_ratios, self.bbox_coordinates))

            outputs_list.append([out_obj, out_bbox])
        return outputs_list



