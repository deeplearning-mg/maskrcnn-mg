from resnet import *
from rpn import *
from anchor_generator import generate_all_anchors


def main():
    x = torch.randn((1, 3, 800, 800))

    anchors = generate_all_anchors(
        feature_map_sizes=[(200, 200), (100, 100), (50, 50), (25, 25), (13, 13)],
        anchor_stride=1,
        feature_strides=(4, 8, 16, 32, 64),
        scales=([32], [64], [128], [256], [512]),
        ratio=(0.5, 1, 2))

    resnet = resnet50()
    out = resnet.forward(x)
    rpn = RPN(out[0], out[1], out[2], out[3], out[4])
    out = rpn.forward()

    i = 0


if __name__ == '__main__':
    main()
