import torch
import torch.nn as nn


def conv3x3(in_planes, out_planes, stride=1, groups=1, padding=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=(3, 3), stride=(stride, stride),
                     padding=padding, groups=groups, bias=False)


def conv1x1(in_planes, out_planes, stride=1):
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=(1, 1), stride=(stride, stride), bias=False)



class BottleneckBlock(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None,
                 base_width=64, norm_layer=None):
        super(BottleneckBlock, self).__init__()

        if norm_layer is None:
            norm_layer = nn.BatchNorm2d

        width = int(planes * (base_width / 64.))
        # Both self.conv2 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv1x1(in_planes=inplanes, out_planes=width)
        self.bn1 = norm_layer(num_features=width)
        self.conv2 = conv3x3(in_planes=width, out_planes=width, stride=stride)
        self.bn2 = norm_layer(num_features=width)
        self.conv3 = conv1x1(in_planes=width, out_planes=planes * self.expansion)
        self.bn3 = norm_layer(num_features=planes * self.expansion)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        skip_connection = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            skip_connection = self.downsample(x)

        out += skip_connection
        out = self.relu(out)

        return out


class FpnResNet(nn.Module):

    def __init__(self, blocks_per_stack_list, zero_init_residual=False,
                 width_per_group=64, norm_layer=None):
        super(FpnResNet, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        self._norm_layer = norm_layer

        self.inplanes = 64
        self.base_width = width_per_group
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=self.inplanes, kernel_size=(7, 7), stride=(2, 2),
                               padding=(3, 3),
                               bias=False)

        self.bn1 = norm_layer(num_features=self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)

        self.C2 = self.build_stack(BottleneckBlock, 64, blocks_per_stack_list[0])
        self.C3 = self.build_stack(BottleneckBlock, 128, blocks_per_stack_list[1], stride=2)
        self.C4 = self.build_stack(BottleneckBlock, 256, blocks_per_stack_list[2], stride=2)
        self.C5 = self.build_stack(BottleneckBlock, 512, blocks_per_stack_list[3], stride=2)

        # WEIGHT INITIALISATION
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves like an identity.
        # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, BottleneckBlock):
                    nn.init.constant_(m.bn3.weight, 0)

    def build_stack(self, block, planes, blocks, stride=1):
        norm_layer = self._norm_layer
        downsample = None

        # build a downsampling skip connection
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                norm_layer(planes * block.expansion),
            )

        layers = []

        layers.append(block(self.inplanes, planes, stride, downsample,
                            self.base_width, norm_layer))
        self.inplanes = planes * block.expansion

        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes,
                                base_width=self.base_width,
                                norm_layer=norm_layer))

        return nn.Sequential(*layers)

    def forward(self, x):

        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        C2 = self.C2(x)
        C3 = self.C3(C2)
        C4 = self.C4(C3)
        C5 = self.C5(C4)

        M5 = conv1x1(in_planes=2048, out_planes=256, stride=1)(C5)
        M4 = conv1x1(in_planes=1024, out_planes=256, stride=1)(C4) + torch.nn.Upsample(scale_factor=2, mode='bilinear')(M5)
        M3 = conv1x1(in_planes=512, out_planes=256, stride=1)(C3) + torch.nn.Upsample(scale_factor=2, mode='bilinear')(M4)
        M2 = conv1x1(in_planes=256, out_planes=256, stride=1)(C2) + torch.nn.Upsample(scale_factor=2, mode='bilinear')(M3)

        P2 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=1)(M2)
        P3 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=1)(M3)
        P4 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=1)(M4)
        P5 = torch.nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=1)(M5)
        P6 = torch.nn.MaxPool2d(kernel_size=(1, 1), stride=2)(P5)
        return P2, P3, P4, P5, P6


def _resnet(layers, pretrained, **kwargs):
    model = FpnResNet(layers, **kwargs)
    if pretrained:
        # state_dict = load_state_dict_from_url(model_urls[arch],
        #                                       progress=progress)
        # model.load_state_dict(state_dict)
        pass
    return model


def resnet50(pretrained=False, **kwargs):
    r"""ResNet-50 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    return _resnet([3, 4, 6, 3], pretrained,
                   **kwargs)


def resnet101(pretrained=False, **kwargs):
    r"""ResNet-101 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    return _resnet([3, 4, 23, 3], pretrained, **kwargs)


def resnet152(pretrained=False, **kwargs):
    r"""ResNet-152 model from
    `"Deep Residual Learning for Image Recognition" <https://arxiv.org/pdf/1512.03385.pdf>`_

    Args:
        pretrained (bool): If True, returns a model pre-trained on ImageNet
        progress (bool): If True, displays a progress bar of the download to stderr
    """
    return _resnet([3, 8, 36, 3], pretrained, **kwargs)
