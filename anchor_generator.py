import numpy as np
import matplotlib.pyplot as plt
from torchvision.models.detection.rpn import AnchorGenerator


def generate_anchors(feature_map_size, anchor_stride, feature_stride, scales, ratios):
    scales, ratios = np.meshgrid(np.array(scales), np.array(ratios))

    heights = scales / np.sqrt(ratios)
    widths = scales * np.sqrt(ratios)

    scales = scales.flatten()
    ratios = ratios.flatten()

    shifts_y = np.arange(0, feature_map_size[0], anchor_stride) * feature_stride
    shifts_x = np.arange(0, feature_map_size[1], anchor_stride) * feature_stride

    shifts_x, shifts_y = np.meshgrid(shifts_x, shifts_y)

    box_widths, box_centers_x = np.meshgrid(widths, shifts_x)
    box_heights, box_centers_y = np.meshgrid(heights, shifts_y)

    box_centers = np.stack([
        box_centers_x,
        box_centers_y
    ], axis=2).reshape([-1, 2])

    box_sizes = np.stack([
        box_heights,
        box_widths,
    ], axis=2).reshape([-1, 2])

    # box coordinates
    # *----
    # |   |
    # |---*

    boxes = np.concatenate([box_centers - 0.5 * box_sizes, box_centers + 0.5 * box_sizes], axis=1)

    # plt.plot(shifts_x, shifts_y, marker='.', color='k', linestyle='none')
    # plt.show()

    return boxes


def generate_all_anchors(feature_map_sizes, anchor_stride, feature_strides, scales, ratio):
    anchors = []
    for i in range(len(feature_map_sizes)):
        anchors.append(generate_anchors(feature_map_sizes[i], anchor_stride, feature_strides[i], scales[i], ratio))
    return np.concatenate(anchors, axis=0)


if __name__ == '__main__':
    all_anchors = generate_all_anchors([(200, 200), (100, 100), (50, 50), (25, 25), (13, 13)], 1, (4, 8, 16, 32, 64),
                                       ([32], [64], [128], [256], [512]), (0.5, 1, 2))
    i=0
